﻿using Cmonitor.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace Cmonitor.Application.Interfaces
{
    public interface ICmonitorDbContext
    {
        DbSet<Coin> Coins { get; set; }

        DbSet<History> Histories { get; set; }

        DbSet<PriceAlert> PriceAlerts { get; set; }

        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }
}
