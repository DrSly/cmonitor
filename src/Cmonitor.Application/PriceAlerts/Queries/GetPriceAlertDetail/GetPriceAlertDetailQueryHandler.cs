﻿using AutoMapper;
using Cmonitor.Application.Exceptions;
using Cmonitor.Application.Interfaces;
using Cmonitor.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace Cmonitor.Application.PriceAlerts.Queries.GetPriceAlertDetail
{
    public class GetPriceAlertDetailQueryHandler : IRequestHandler<GetPriceAlertDetailQuery, PriceAlertDetailModel1>
    {
        private readonly ICmonitorDbContext _context;
        private readonly IMapper _mapper;

        public GetPriceAlertDetailQueryHandler(ICmonitorDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<PriceAlertDetailModel1> Handle(GetPriceAlertDetailQuery request, CancellationToken cancellationToken)
        {
            var entity = await _context.PriceAlerts
                .Include(p => p.Coin)
                .FirstOrDefaultAsync(p => p.Id == request.Id);  // Since Include changes the method's return type, we can't use FindAsync

            if (entity == null)
            {
                throw new NotFoundException(nameof(PriceAlert), request.Id);
            }

            return _mapper.Map<PriceAlertDetailModel1>(entity);
        }
    }
}
