﻿using MediatR;

namespace Cmonitor.Application.PriceAlerts.Queries.GetPriceAlertDetail
{
    public class GetPriceAlertDetailQuery : IRequest<PriceAlertDetailModel1>
    {
        public int Id { get; set; }
    }
}
