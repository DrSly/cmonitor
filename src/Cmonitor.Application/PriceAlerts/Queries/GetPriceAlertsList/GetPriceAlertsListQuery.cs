﻿using MediatR;

namespace Cmonitor.Application.PriceAlerts.Queries.GetPriceAlertsList
{
    public class GetPriceAlertsListQuery : IRequest<PriceAlertsListViewModel>
    {
    }
}
