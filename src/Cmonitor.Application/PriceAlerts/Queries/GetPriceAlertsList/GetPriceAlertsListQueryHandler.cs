﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Cmonitor.Application.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace Cmonitor.Application.PriceAlerts.Queries.GetPriceAlertsList
{
    public class GetPriceAlertsListQueryHandler : IRequestHandler<GetPriceAlertsListQuery, PriceAlertsListViewModel>
    {
        private readonly ICmonitorDbContext _context;
        private readonly IMapper _mapper;

        public GetPriceAlertsListQueryHandler(ICmonitorDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<PriceAlertsListViewModel> Handle(GetPriceAlertsListQuery request, CancellationToken cancellationToken)
        {
            return new PriceAlertsListViewModel
            {
                PriceAlerts = await _context.PriceAlerts.ProjectTo<PriceAlertLookupModel>(_mapper.ConfigurationProvider).ToListAsync(cancellationToken)
            };
        }
    }
}
