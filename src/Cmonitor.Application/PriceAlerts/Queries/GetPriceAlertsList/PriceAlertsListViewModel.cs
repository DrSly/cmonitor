﻿using System.Collections.Generic;

namespace Cmonitor.Application.PriceAlerts.Queries.GetPriceAlertsList
{
    public class PriceAlertsListViewModel
    {
        public IList<PriceAlertLookupModel> PriceAlerts { get; set; }
    }
}
