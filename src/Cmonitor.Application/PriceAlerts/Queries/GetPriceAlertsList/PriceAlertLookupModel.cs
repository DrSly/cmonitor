﻿using AutoMapper;
using Cmonitor.Application.Extensions;
using Cmonitor.Application.Interfaces.Mapping;
using Cmonitor.Domain.Entities;

namespace Cmonitor.Application.PriceAlerts.Queries.GetPriceAlertsList
{
    public class PriceAlertLookupModel : IHaveCustomMapping
    {
        public string Id { get; set; }
        public string Direction { get; set; }
        public string Limit { get; set; }
        public string CoinName { get; set; }

        public void CreateMappings(Profile configuration)
        {
            configuration.CreateMap<PriceAlert, PriceAlertLookupModel>()
                .ForMember(cDTO => cDTO.Id, opt => opt.MapFrom(c => c.Id))
                .ForMember(cDTO => cDTO.Direction, opt => opt.MapFrom(c => c.Direction.ToDescriptionString()))
                .ForMember(cDTO => cDTO.Limit, opt => opt.MapFrom(c => c.Limit))
                .ForMember(pDTO => pDTO.CoinName, opt => opt.MapFrom(p => p.Coin != null ? p.Coin.Code : string.Empty));
        }
    }
}