﻿using MediatR;

namespace Cmonitor.Application.PriceAlerts.Commands.UpdatePriceAlert
{
    public class UpdatePriceAlertCommand : IRequest<UpdatePriceAlertViewModel>
    {
        public int Id { get; set; }
        public int Direction { get; set; }
        public double Limit { get; set; }
        public string EmailAddress { get; set; }
        public int CoinId { get; set; }
    }
}
