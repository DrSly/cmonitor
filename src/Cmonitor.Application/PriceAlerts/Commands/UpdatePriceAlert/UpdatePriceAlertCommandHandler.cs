﻿using AutoMapper;
using Cmonitor.Application.Exceptions;
using Cmonitor.Application.Interfaces;
using Cmonitor.Domain.Entities;
using Cmonitor.Domain.Enum;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Cmonitor.Application.PriceAlerts.Commands.UpdatePriceAlert
{
    public class UpdatePriceAlertCommandHandler : IRequestHandler<UpdatePriceAlertCommand, UpdatePriceAlertViewModel>
    {
        private readonly ICmonitorDbContext _context;
        private readonly IMapper _mapper;

        public UpdatePriceAlertCommandHandler(ICmonitorDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<UpdatePriceAlertViewModel> Handle(UpdatePriceAlertCommand request, CancellationToken cancellationToken)
        {
            var entity = await _context.PriceAlerts
                .FindAsync(request.Id);

            if (entity == null)
            {
                throw new NotFoundException(nameof(PriceAlert), request.Id);
            }

            entity.Id = request.Id;
            entity.Direction = (EDirection)request.Direction;
            entity.Limit = request.Limit;
            entity.EmailAddress = request.EmailAddress;
            entity.CoinId = request.CoinId;

            await _context.SaveChangesAsync(cancellationToken);

            return _mapper.Map<UpdatePriceAlertViewModel>(entity);
        }

    }
}
