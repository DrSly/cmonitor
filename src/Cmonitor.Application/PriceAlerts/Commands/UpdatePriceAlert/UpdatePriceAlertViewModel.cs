﻿using AutoMapper;
using Cmonitor.Application.Interfaces.Mapping;
using Cmonitor.Domain.Entities;

namespace Cmonitor.Application.PriceAlerts.Commands.UpdatePriceAlert
{
    public class UpdatePriceAlertViewModel : IHaveCustomMapping
    {
        public int Id { get; set; }
        public string Direction { get; set; }
        public double Limit { get; set; }
        public string EmailAddress { get; set; }
        public int CoinId { get; set; }

        public void CreateMappings(Profile configuration)
        {
            configuration.CreateMap<PriceAlert, UpdatePriceAlertViewModel>()
                .ForMember(pDTO => pDTO.Id, opt => opt.MapFrom(p => p.Id))
                .ForMember(pDTO => pDTO.Direction, opt => opt.MapFrom(p => p.Direction))
                .ForMember(pDTO => pDTO.Limit, opt => opt.MapFrom(p => p.Limit))
                .ForMember(pDTO => pDTO.EmailAddress, opt => opt.MapFrom(p => p.EmailAddress))
                .ForMember(pDTO => pDTO.CoinId, opt => opt.MapFrom(p => p.CoinId));
        }

    }
}
