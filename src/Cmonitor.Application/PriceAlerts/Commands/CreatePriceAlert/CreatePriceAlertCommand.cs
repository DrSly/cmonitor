﻿using MediatR;

namespace Cmonitor.Application.PriceAlerts.Commands.CreatePriceAlert
{
    public class CreatePriceAlertCommand : IRequest<PriceAlertDetailModel>
    {
        public int Direction { get; set; }
        public double Limit { get; set; }
        public string EmailAddress { get; set; }
        public int CoinId { get; set; }
    }
}
