﻿using AutoMapper;
using Cmonitor.Application.Interfaces;
using Cmonitor.Domain.Entities;
using Cmonitor.Domain.Enum;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Cmonitor.Application.PriceAlerts.Commands.CreatePriceAlert
{
    public class CreatePriceAlertCommandHandler : IRequestHandler<CreatePriceAlertCommand, PriceAlertDetailModel>
    {
        private readonly ICmonitorDbContext _context;
        private readonly IMapper _mapper;

        public CreatePriceAlertCommandHandler(ICmonitorDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<PriceAlertDetailModel> Handle(CreatePriceAlertCommand request, CancellationToken cancellationToken)
        {
            var entity = new PriceAlert
            {
                Direction = (EDirection)request.Direction,
                Limit = request.Limit,
                EmailAddress = request.EmailAddress,
                CoinId = request.CoinId
            };

            _context.PriceAlerts.Add(entity);

            await _context.SaveChangesAsync(cancellationToken);

            return _mapper.Map<PriceAlertDetailModel>(entity);
        }

    }
}
