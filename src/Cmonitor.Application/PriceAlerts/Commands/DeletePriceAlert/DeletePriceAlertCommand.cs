﻿using MediatR;

namespace Cmonitor.Application.PriceAlerts.Commands.DeletePriceAlert
{
    public class DeletePriceAlertCommand : IRequest<DeletePriceAlertViewModel>
    {
        public int Id { get; set; }
    }
}
