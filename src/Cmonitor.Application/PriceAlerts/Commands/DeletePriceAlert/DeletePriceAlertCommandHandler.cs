﻿using AutoMapper;
using Cmonitor.Application.Exceptions;
using Cmonitor.Application.Interfaces;
using Cmonitor.Domain.Entities;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Cmonitor.Application.PriceAlerts.Commands.DeletePriceAlert
{
    public class DeletePriceAlertCommandHandler : IRequestHandler<DeletePriceAlertCommand, DeletePriceAlertViewModel>
    {
        private readonly ICmonitorDbContext _context;
        private readonly IMapper _mapper;

        public DeletePriceAlertCommandHandler(ICmonitorDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<DeletePriceAlertViewModel> Handle(DeletePriceAlertCommand request, CancellationToken cancellationToken)
        {
            var entity = await _context.PriceAlerts
                .FindAsync(request.Id);

            if (entity == null)
            {
                throw new NotFoundException(nameof(PriceAlert), request.Id);
            }

            _context.PriceAlerts.Remove(entity);

            await _context.SaveChangesAsync(cancellationToken);

            return _mapper.Map<DeletePriceAlertViewModel>(entity);
        }
    }
}
