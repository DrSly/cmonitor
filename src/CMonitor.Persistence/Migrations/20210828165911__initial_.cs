﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CMonitor.Persistence.Migrations
{
    public partial class _initial_ : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "dbo");

            migrationBuilder.CreateTable(
                name: "Coins",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Code = table.Column<string>(maxLength: 20, nullable: false),
                    Watch = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Coins", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Histories",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Time = table.Column<DateTime>(nullable: false),
                    Price = table.Column<double>(nullable: false),
                    CoinId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Histories", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Histories_Coins_CoinId",
                        column: x => x.CoinId,
                        principalSchema: "dbo",
                        principalTable: "Coins",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PriceAlerts",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Direction = table.Column<byte>(nullable: false),
                    Limit = table.Column<double>(nullable: false),
                    EmailAddress = table.Column<string>(maxLength: 200, nullable: false),
                    CoinId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PriceAlerts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PriceAlerts_Coins_CoinId",
                        column: x => x.CoinId,
                        principalSchema: "dbo",
                        principalTable: "Coins",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Histories_CoinId",
                schema: "dbo",
                table: "Histories",
                column: "CoinId");

            migrationBuilder.CreateIndex(
                name: "IX_PriceAlerts_CoinId",
                schema: "dbo",
                table: "PriceAlerts",
                column: "CoinId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Histories",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "PriceAlerts",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "Coins",
                schema: "dbo");
        }
    }
}
