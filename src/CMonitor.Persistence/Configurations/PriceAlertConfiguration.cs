﻿using Cmonitor.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CMonitor.Persistence.Configurations
{
    public class PriceAlertConfiguration : IEntityTypeConfiguration<PriceAlert>
    {
        public void Configure(EntityTypeBuilder<PriceAlert> builder)
        {
            builder.Property(e => e.Direction)
                .IsRequired();

            builder.Property(e => e.Limit)
                .IsRequired();

            builder.Property(e => e.EmailAddress)
                .IsRequired()
                .HasMaxLength(200);
        }
    }
}
