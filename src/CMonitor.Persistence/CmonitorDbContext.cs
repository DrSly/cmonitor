﻿using Cmonitor.Application.Interfaces;
using Cmonitor.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace CMonitor.Persistence
{
    public class CmonitorDbContext : DbContext, ICmonitorDbContext
    {
        public CmonitorDbContext(DbContextOptions<CmonitorDbContext> options)
            : base(options)
        {
        }

        public DbSet<Coin> Coins { get; set; }
        public DbSet<History> Histories { get; set; }
        public DbSet<PriceAlert> PriceAlerts { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.HasDefaultSchema("dbo");

            builder.ApplyConfigurationsFromAssembly(typeof(CmonitorDbContext).Assembly);

        }

    }
}
