﻿using Cmonitor.Domain.Enum;

namespace Cmonitor.Domain.Entities
{
    public class PriceAlert
    {
        public int Id { get; set; }
        public EDirection Direction { get; set; }
        public double Limit { get; set; }
        public string EmailAddress { get; set; }
        public int CoinId { get; set; }
        public Coin Coin { get; set; }
    }
}
