﻿using System.Collections.Generic;

namespace Cmonitor.Domain.Entities
{
    public class Coin
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public bool Watch { get; set; }
        public ICollection<History> Histories { get; private set; } = new HashSet<History>();
        public ICollection<PriceAlert> PriceAlerts { get; private set; } = new HashSet<PriceAlert>();
    }
}
