﻿using System;

namespace Cmonitor.Domain.Entities
{
    public class History
    {
        public int Id { get; set; }
        public DateTime Time { get; set; }
        public double Price { get; set; }
        public int CoinId { get; set; }
        public Coin Coin { get; set; }
    }
}
