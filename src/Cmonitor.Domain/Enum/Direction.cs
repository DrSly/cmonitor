﻿using System.ComponentModel;

namespace Cmonitor.Domain.Enum
{
    public enum EDirection : byte
    {
        [Description("[Up]")]
        Up = 1,

        [Description("[Down]")]
        Down = 2
    }
}
