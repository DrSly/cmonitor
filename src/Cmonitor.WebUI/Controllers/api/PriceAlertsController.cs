﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cmonitor.Application.PriceAlerts.Commands.CreatePriceAlert;
using Cmonitor.Application.PriceAlerts.Commands.DeletePriceAlert;
using Cmonitor.Application.PriceAlerts.Commands.UpdatePriceAlert;
using Cmonitor.Application.PriceAlerts.Queries.GetPriceAlertDetail;
using Cmonitor.Application.PriceAlerts.Queries.GetPriceAlertsList;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Cmonitor.WebUI.Controllers.api
{
    [Route("api/[controller]")]
    [ApiController]
    public class PriceAlertsController : MediatorControllerBase
    {
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<PriceAlertsListViewModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetAll()
        {
            return Ok(await Mediator.Send(new GetPriceAlertsListQuery()));
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(PriceAlertDetailModel1), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Get(int id)
        {
            return Ok(await Mediator.Send(new GetPriceAlertDetailQuery { Id = id }));
        }

        [HttpPost]
        [ProducesResponseType(typeof(PriceAlertDetailModel), StatusCodes.Status200OK)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Create([FromBody] CreatePriceAlertCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        [HttpPut("{id}")]
        [ProducesResponseType(typeof(UpdatePriceAlertViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Update([FromBody] UpdatePriceAlertCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(DeletePriceAlertViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Delete(int id)
        {
            return Ok(await Mediator.Send(new DeletePriceAlertCommand { Id = id }));
        }

    }
}